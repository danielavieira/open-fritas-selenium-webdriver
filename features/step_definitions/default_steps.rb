# encoding: utf-8

Dado(/^que eu esteja no site do Abril ID$/) do
  @browser.get "http://id.abril.com.br/"
end

Dado(/^eu preencho o campo usuário com o valor "(.*?)"$/) do |login|
  @browser.find_element(:id, "login").send_keys login
end

Dado(/^eu preencho o campo senha com o valor "(.*?)"$/) do |senha|
    @browser.find_element(:id, "senha").send_keys senha
end

Dado(/^eu clico no botao Entrar$/) do
    @browser.find_element(:id, "loginEnviar").click
end

Entao(/^eu devo ver o nome completo do usuário "(.*?)"$/) do |nome_completo|
	sleep 4
  fail "Nome completo nao encontrado." unless @browser.find_element(:class, "userdata-box-realname").text == nome_completo
end