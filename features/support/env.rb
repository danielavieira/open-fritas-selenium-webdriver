
require 'rspec-expectations'


HUB = ENV["HUB"] ||"browserstack"
BROWSER = ENV["BROWSER"] ||"firefox"
BROWSER_VERSION = ENV["BROWSER_VERSION"] ||"22.0"
SO = ENV["SO"] ||"WINDOWS"