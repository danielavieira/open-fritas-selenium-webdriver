# language: pt

Funcionalidade: Realizar Login
	Como usuário do abril id
	Eu quero fazer login
	Para acessar os sites da abril

	Cenário: Realizar login
		Dado que eu esteja no site do Abril ID
		E eu preencho o campo usuário com o valor "daenerys@mailinator.net"
		E eu preencho o campo senha com o valor "123456"
		E eu clico no botao Entrar
		Entao eu devo ver o nome completo do usuário "Daenerys Targaryen"
