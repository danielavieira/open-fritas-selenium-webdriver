#!/bin/bash

cucumber features/recuperar_login.feature HUB=testingbot BROWSER=internet_explorer BROWSER_VERSION=7.0 SO=WINDOWS
cucumber features/recuperar_login.feature HUB=testingbot BROWSER=internet_explorer BROWSER_VERSION=8.0 SO=WINDOWS
cucumber features/recuperar_login.feature HUB=testingbot BROWSER=internet_explorer BROWSER_VERSION=9.0 SO=WINDOWS
cucumber features/recuperar_login.feature HUB=testingbot BROWSER=internet_explorer BROWSER_VERSION=10.0 SO=WINDOWS
cucumber features/recuperar_login.feature HUB=testingbot BROWSER=chrome BROWSER_VERSION=25.0 SO=LINUX
cucumber features/recuperar_login.feature HUB=testingbot BROWSER=firefox BROWSER_VERSION=21.0 SO=LINUX
cucumber features/recuperar_login.feature HUB=testingbot BROWSER=safari BROWSER_VERSION=6.0 SO=MAC
